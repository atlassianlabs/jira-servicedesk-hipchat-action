$(function () {
    AP.require(["events"], function(events) {
        var automation = new AP.jiraServiceDesk.Automation();

        automation.getConfig(function(config) {
            if (config) {
                $("#room-id").val(config.roomId);
                $("#notification-token").val(config.notificationToken);
                $("#message").val(config.message);
            }
        });

        events.on("jira-servicedesk-automation-action-serialize", function() {
            automation.serialize({
                roomId: $("#room-id").val(),
                notificationToken: $("#notification-token").val(),
                message: $("#message").val()
            });
        });

        events.on("jira-servicedesk-automation-action-validate", function() {
            var valid = true;

            if (!$("#room-id").val()) {
                $("#room-id").parent().append("<p>Room ID can't be empty</p>");
                valid = false;
            }

            if (!$("#notification-token").val()) {
                $("#notification-token").parent().append("<p>Notification token can't be empty</p>");
                valid = false;
            }

            if (!$("#message").val()) {
                $("#message").parent().append("<p>Message can't be empty</p>");
                valid = false;
            }

            automation.validate(valid);
        });
    });
});
