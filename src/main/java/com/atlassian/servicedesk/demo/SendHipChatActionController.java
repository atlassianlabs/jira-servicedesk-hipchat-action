package com.atlassian.servicedesk.demo;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;

/**
 * Exposes route to webhook and automation configuration form
 */
@Controller
@RequestMapping("/send-hipchat")
public class SendHipChatActionController {

    private static Logger log = LoggerFactory.getLogger(SendHipChatActionController.class);

    @Autowired
    private SendHipChatActionService sendHipChatActionService;

    private final JsonParser jsonParser = new JsonParser();

    /**
     * Handles inbound automation action webhook request from JSD
     */
    @RequestMapping(value = "/webhook", method = RequestMethod.POST)
    public ResponseEntity webhookHandler(@RequestBody String payloadString) throws IOException {
        JsonObject payload = jsonParser.parse(payloadString).getAsJsonObject();
        JsonObject actionConfiguration = payload.getAsJsonObject("action").getAsJsonObject("configuration");
        JsonObject jiraIssue = payload.getAsJsonObject("issue");
        JsonObject issueFields = jiraIssue.getAsJsonObject("fields");

        String roomId = actionConfiguration.get("roomId").getAsString();
        String notificationToken = actionConfiguration.get("notificationToken").getAsString();
        String message = actionConfiguration.get("message").getAsString();
        String jiraIssueKey = jiraIssue.get("key").getAsString();
        String jiraSummary = issueFields.get("summary").getAsString();
        String jiraDescription = issueFields.get("description").getAsString();

        sendHipChatActionService.notify(roomId,
                                        notificationToken,
                                        message,
                                        jiraIssueKey,
                                        jiraSummary,
                                        jiraDescription);

        return ResponseEntity.ok().build();
    }

    /**
     * Serve action configuration form, to be rendered inside an iframe by JSD. Spring resolves the view name into /src/main/resources/templates/config-form.html.
     */
    @RequestMapping(value = "/config-form", method = RequestMethod.GET)
    public String configForm() {
        return "config-form";
    }
}
