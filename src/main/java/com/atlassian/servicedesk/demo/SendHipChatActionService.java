package com.atlassian.servicedesk.demo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Invokes HipChat API to send notification message
 */
@Component
public class SendHipChatActionService {

    private static final Logger log = LoggerFactory.getLogger(SendHipChatActionService.class);

    public void notify(String roomId, String notificationToken, String message, String jiraIssueKey, String jiraSummary, String jiraDescription) throws IOException {
        String hipChatApiUrl = String.format("https://api.hipchat.com/v2/room/%s/notification", roomId);
        HipChatNotification hipChatNotification = new HipChatNotification(String.format("%s %s %s %s", message.trim(), jiraIssueKey, jiraSummary, jiraDescription));

        log.info("Reqeust: POST {}", hipChatApiUrl);

        HttpResponse response = Request.Post(hipChatApiUrl)
                .addHeader("Authorization", "Bearer " + notificationToken)
                .body(new StringEntity(hipChatNotification.toJsonString(), ContentType.APPLICATION_JSON))
                .execute().returnResponse();

        log.info("Response: {} {}", response.getStatusLine().getStatusCode(), response.getStatusLine().getReasonPhrase());
    }

    /**
     * Data class to hold object before serialized to JSON
     */
    static class HipChatNotification {

        private final String color = "green";
        private final String message;
        private final boolean notify = false;
        private final String message_format = "text";

        public HipChatNotification(String message) {
            this.message = message;
        }

        @JsonIgnore
        public String toJsonString() {
            return new Gson().toJson(this);
        }

        public String getColor() {
            return color;
        }

        public String getMessage() {
            return message;
        }

        public boolean isNotify() {
            return notify;
        }

        public String getMessage_format() {
            return message_format;
        }

    }
}
