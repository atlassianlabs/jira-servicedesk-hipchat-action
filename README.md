JSD HipChat Automation Action
=============================

A demo of JIRA Service Desk (JSD) Connect automation action to send notification to a HipChat room. This add-on uses 
[Atlassian Connect Spring Boot](https://bitbucket.org/atlassian/atlassian-connect-spring-boot) library.

To run and install the add-on
-------------------------------
1. Grab a JIRA cloud instance (you can use free-unlimited [cloud dev instance](https://go.atlassian.com/cloud-dev)).
2. Run `ngrok http 8080` to setup tunnel to your local machine. Take note of the ngrok url (eg: **https://a1b2c3d4.ngrok.io**). 
Make sure you have [ngrok installed](https://ngrok.com/download).
3. Run `mvn clean spring-boot:run -Drun.jvmArguments="-Dbase.url=https://a1b2c3d4.ngrok.io"` (ensure you have jdk and maven installed and port 8080 is free)
4. In JIRA, go to Adminisrations > Add-ons > Manage add-ons > Upload add-on > input the url **https://a1b2c3d4.ngrok.io/atlassian-connect.json**

To use the addon
----------------
1. As an administrator, go to a Service Desk project > Project settings > Automation
2. Add a custom rule with arbitrary WHEN and IF (eg: WHEN issue created, IF Any items)
3. Add a **Send HipChat Notification** THEN action. Follow the instruction to get **HipChat Room ID** and **Notification token**. Confirm and save the rule.
4. Trigger your rule (eg: Raise a request using the sidebar link), see the notification popup in your HipChat room :)

Note
----
Auto-installation and tunneling for Atlassian Connect Spring Boot is not yet implemented. You can track down these two issues:

* https://ecosystem.atlassian.net/browse/ACSPRING-1
* https://ecosystem.atlassian.net/browse/ACSPRING-2

Also read these docs
--------------------
* [JSD automation action developer guide](https://developer.atlassian.com/jiracloud/guide-implementing-automation-actions-41223090.html).
* [JSD automation feature documentation](https://confluence.atlassian.com/servicedeskcloud/automating-your-service-desk-732528900.html).